SHELL=/bin/sh

projectid=48109889
TAG?=v0.2.1

.PHONY: built

built:
	@echo "Downloading built artifacts."
	curl --location "https://gitlab.com/api/v4/projects/$(projectid)/jobs/artifacts/$(TAG)/raw/build.zip?job=build" --output build.zip
	unzip artifacts.zip
	rm artifacts.zip
